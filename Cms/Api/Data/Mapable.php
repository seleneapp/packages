<?php

/*
 * This File is part of the Selene\Package\Cms\Api\Data package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Api\Data;

/**
 * @interface Mapable
 * @package Selene\Package\Cms\Api\Data
 * @version $Id$
 */
interface Mapable
{
    public function getDataMap();
}
