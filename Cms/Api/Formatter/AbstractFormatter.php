<?php

/*
 * This File is part of the Selene\Package\Cms\Api\Formatter package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Api\Formatter;

/**
 * @class AbstractFormatter
 * @package Selene\Package\Cms\Api\Formatter
 * @version $Id$
 */
abstract class AbstractFormatter implements FormatterInterface
{
    /**
     * root
     *
     * @var string
     */
    private $root;

    /**
     * Constructor.
     *
     * @param string $root
     */
    public function __construct($root = 'root')
    {
        $this->root = $root;
    }

    /**
     * getRootName
     *
     * @return string
     */
    protected function getRootName()
    {
        return $this->root;
    }
}
