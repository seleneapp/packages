<?php

/*
 * This File is part of the Selene\Package\Cms\Api\Formatter package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Api\Formatter;

/**
 * @class DelegatingFormatter
 * @package Selene\Package\Cms\Api\Formatter
 * @version $Id$
 */
class DelegatingFormatter implements FormatterInterface
{
    private $lastMatch;
    private $formatters;

    /**
     * Constructor
     *
     * @param array $formatters
     */
    public function __construct(array $formatters = [])
    {
        $this->lastMatch = new \SplStack;
        $this->setFormatters($formatters);
    }

    /**
     * setFormatters
     *
     * @param array $formatters
     *
     * @return void
     */
    public function setFormatters(array $formatters)
    {
        $this->formatters = [];

        foreach ($formatters as $formatter) {
            $this->addFormatter($formatter);
        }
    }

    /**
     * addformatter
     *
     * @param FormatterInterface $formatter
     *
     * @return void
     */
    public function addformatter(FormatterInterface $formatter)
    {
        $this->formatters[] = $formatter;
    }

    public function supports($type)
    {
        if (null !== ($this->resolve($type))) {
            return true;
        }

        return false;
    }

    /**
     * format
     *
     *
     * @return void
     */
    public function format($data)
    {
        if ($this->lastMatch->count()) {
            return $this->lastMatch->pop()->format($data);
        }
    }

    /**
     * resolve
     *
     * @param string $type
     *
     * @return ForammterInterface|null
     */
    public function resolve($type)
    {
        foreach ($this->formatters as $formatter) {
            if ($formatter->supports($type)) {
                $this->lastMatch->push($formatter);

                return $formatter;
            }
        }
    }
}
