<?php

/*
 * This File is part of the Selene\Package\Cms\Api\Formatter package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Api\Formatter;

/**
 * @class FormatterInterface
 * @package Selene\Package\Cms\Api\Formatter
 * @version $Id$
 */
interface FormatterInterface
{
    /**
     * format
     *
     * @param mixed $data
     *
     * @return string
     */
    public function format($data);

    /**
     * supports
     *
     * @param string $type
     *
     * @return boolean
     */
    public function supports($type);
}
