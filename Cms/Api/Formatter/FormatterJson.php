<?php

/*
 * This File is part of the Selene\Package\Cms\Api\Formatter package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Api\Formatter;

/**
 * @class FormatterJson
 * @package Selene\Package\Cms\Api\Formatter
 * @version $Id$
 */
class FormatterJson extends AbstractFormatter
{
    /**
     * {@inheritdoc}
     */
    public function format($data)
    {
        return json_encode([$this->getRootName() => $data]);
    }

    /**
     * {@inheritdoc}
     */
    public function supports($type)
    {
        return 'json' === $type || in_array($type, ['application/json', 'text/json']);
    }
}
