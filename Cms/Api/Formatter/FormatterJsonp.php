<?php

/*
 * This File is part of the Selene\Package\Cms\Api\Formatter package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Api\Formatter;

/**
 * @class FormatterJson
 * @package Selene\Package\Cms\Api\Formatter
 * @version $Id$
 */
class FormatterJsonp extends FormatterJson
{
    /**
     * callback
     *
     * @var string
     */
    private $callback;

    /**
     * Constructor.
     *
     * @param string $callback
     * @param string $root
     */
    public function __construct($callback = 'callback', $root = 'root')
    {
        $this->callback = $callback;

        parent::__construct($root);
    }

    /**
     * {@inheritdoc}
     */
    public function format($data)
    {
        return sprintf('var %s = function () { return %s};', $this->getCallback(), parent::format($data));
    }

    /**
     * getCallback
     *
     * @return string
     */
    protected function getCallback()
    {
        return $this->callback ?: 'callback';
    }

    /**
     * {@inheritdoc}
     */
    public function supports($type)
    {
        return in_array($type, ['js', 'jsonp', 'application/javascript', 'text/javascript']);
    }
}
