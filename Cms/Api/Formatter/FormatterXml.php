<?php

/*
 * This File is part of the Selene\Package\Cms\Api\Formatter package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Api\Formatter;

use \Selene\Module\Xml\Writer;

/**
 * @class FormatterXml
 * @package Selene\Package\Cms\Api\Formatter
 * @version $Id$
 */
class FormatterXml extends AbstractFormatter
{
    /**
     * writer
     *
     * @var Writer
     */
    private $writer;

    /**
     * Constructor
     *
     * @param Writer $writer
     * @param string $root
     */
    public function __construct(Writer $writer = null, $root = 'data')
    {
        $this->writer = $writer ?: new Writer;

        parent::__construct($root);
    }

    /**
     * {@inheritdoc}
     */
    public function supports($type)
    {
        return 'xml' === $type || in_array($type, ['application/xml', 'text/xml']);
    }

    /**
     * {@inheritdoc}
     */
    public function format($data)
    {
        return $this->getWriter()->dump($data, $this->getRootName());
    }

    /**
     * getWriter
     *
     * @return Writer
     */
    protected function getWriter()
    {
        return $this->writer;
    }
}
