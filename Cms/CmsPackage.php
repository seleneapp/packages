<?php

/*
 * This File is part of the Selene\Package\Cms package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms;

use \DOMDocument;
use \Selene\Module\Package\Package;
use \Selene\Adapter\Kernel\ApplicationInterface;
use \Selene\Module\DI\BuilderInterface;
use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Processor\ProcessorInterface;
use \Selene\Package\Cms\Process\CollectPackageMap;
use \Selene\Package\Cms\Process\PrepareExtensions;
use \Selene\Package\Cms\Extension\Extension;
use \Selene\Package\Cms\Extension\Repository;
use \Selene\Adapter\Translation\MessageLoader;
use \Symfony\Component\HttpFoundation\RedirectResponse;
use \Selene\Package\Cms\Extension\Finder;
use \Selene\Package\Cms\Extension\Parser;
use \Selene\Package\Cms\Extension\Loader;
use \Selene\Module\Filesystem\Filereader;
use \Selene\Module\Package\PackageRepositoryInterface;

/**
 * @class CmsPackage extends Package
 * @see Package
 *
 * @package Selene\Package\Cms
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class CmsPackage extends Package
{
    private $extensions;

    /**
     * {@inheritdoc}
     */
    public function build(BuilderInterface $builder)
    {
        $this->registerPostBuilds($builder);
        $this->prepareRepository($container = $builder->getContainer());

        $builder->getProcessor()
            ->add(new CollectPackageMap, ProcessorInterface::AFTER_REMOVE);
    }

    /**
     * boot
     *
     * @param ApplicationInterface $app
     *
     * @return void
     */
    public function boot(ApplicationInterface $app)
    {
        $container = $app->getContainer();

        $this->prepareRepository($container);
        $this->bootExtensions($app);
        $this->bootTranslator($app->getContainer());
        $this->registerExtensionWrapper($container);
    }

    /**
     * {@inheritdoc}
     */
    public function requires()
    {
        return ['framework', 'twig'];
    }

    /**
     * getExtensionRepository
     *
     * @param ContainerInterface $container
     * @param Repository $repo
     *
     * @return Repository
     */
    private function getExtensionRepository(ContainerInterface $container, PackageRepositoryInterface $repo = null)
    {
        if (null === $repo) {

            // loading the extension namespaces and files from the extension
            // base directory.

            $loader = new Loader(
                $repo = new Repository([], $packages = $container->get('app')->getPackages()),
                $container->get('app.classloader'),
                $container->getParameter('cms.extension.classes'),
                $container->getParameter('cms.extension.autoload')
            );

            $loader->load();
        }

        return $repo;
    }

    /**
     * prepareRepository
     *
     * @param ContainerInterface $container
     *
     * @return void
     */
    private function prepareRepository(ContainerInterface $container)
    {
        $this->extensions = $this->getExtensionRepository($container, $this->extensions);
    }

    /**
     * registerPostBuilds
     *
     * @param Builderinterface $builder
     *
     * @return void
     */
    private function registerPostBuilds(Builderinterface $builder)
    {
        $builder->getContainer()->get('app')->onPackagesPostBuild(function () use (&$builder) {
            $this->extensions->build($builder);
        });
    }

    /**
     * getExtensionResourceFile
     *
     * @param ContainerInteface $container
     *
     * @return void
     */
    private function getExtensionResourceFile(ContainerInterface $container)
    {
        return $container->getParameter('app.root') . '/packages/extension.php';
    }

    /**
     * bootExtensions
     *
     * @param ApplicationInterface $app
     *
     * @return void
     */
    private function bootExtensions(ApplicationInterface $app)
    {
        $app->getContainer()->inject('cms.extension.repository', $this->extensions);
        $this->extensions->boot($app);
    }

    /**
     * bootTranslator
     *
     * @param ContainerInterface $container
     *
     * @return void
     */
    private function bootTranslator(ContainerInterface $container)
    {
        $paths = $container->getParameter('cms.extension.paths');

        array_unshift($paths, $this->getPath());

        (new MessageLoader($container->get('translation.translator'), $paths, 'Resources/lang'))->load();
    }

    /**
     * registerExtensionWrapper
     *
     * @param ApplicationInterface $app
     *
     * @return void
     */
    private function registerExtensionWrapper(ContainerInterface $container)
    {
        if (false === stream_wrapper_register('extension', $class = __NAMESPACE__.'\Filesystem\ResourceStream')) {
            throw new \RuntimeException('Cannot register resource stream.');
        }

        $class::setMapInstantiator(function () use ($container) {
            return $container->getParameter('extension.resources');
        });
    }
}
