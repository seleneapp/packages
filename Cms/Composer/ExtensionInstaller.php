<?php

/*
 * This File is part of the Selene\Package\Cms package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Composer;

use \Composer\Composer;
use \Composer\IO\IOInterface;
use \Composer\Package\PackageInterface;
use \Composer\Installer\LibraryInstaller;
use \Composer\Repository\InstalledRepositoryInterface;

/**
 * @class ExtensionInstaller
 * @package Selene\Package\Cms\Composer
 * @version $Id$
 */
class ExtensionInstaller extends LibraryInstaller
{
    const S_TYPE = 'cms_extension';

    /**
     * Constructor.
     *
     * @param IOInterface $io
     * @param Composer $composer
     */
    public function __construct(IOInterface $io, Composer $composer, $vendorDir = null)
    {
        parent::__construct($io, $composer, static::S_TYPE);
    }

    /**
     * {@inheritdoc}
     */
    public function supports($packageType)
    {
        return static::S_TYPE === $packageType;
    }
}
