<?php

/**
 * This File is part of the Selene\Package\Cms package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms;

use \Selene\Package\Cms\Menu\ConfigParser;
use \Selene\Module\DI\BuilderInterface;
use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\Package\PackageConfiguration;
use \Selene\Module\Config\Validator\Nodes\RootNode;
use \Selene\Module\Routing\RouteBuilder;
use \Selene\Module\Routing\RouteCollection;
use \Selene\Adapter\Translation\MessageFinder;
use \Selene\Package\Cms\Extension\Finder;
use \Selene\Package\Cms\Extension\Parser;

/**
 * @class Config extends PackageConfiguration
 * @see PackageConfiguration
 *
 * @package Selene\Package\Cms
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Config extends PackageConfiguration
{
    public function setup(BuilderInterface $builder, array $config)
    {
        $this->setupRoutes($builder, $config['uri']);
        $this->setupMenu();

        $this->setParameter('cms.extension.paths', []);
        $this->setParameter('cms.base_uri', $config['uri']);
        $this->setParameter(
            'cms.extension.dir',
            $dir = $this->expandPath($builder->getContainer()->getParameters()->resolveParam($config['extension_dir']))
        );

        $this->setExtensionNamespaces(
            $builder->getContainer(),
            new Parser(new Finder($dir), $builder->getContainer()->get('app.classloader'))
        );
    }

    protected function setUpMenu()
    {
        $parser = new ConfigParser($this->getResourcePath().'/menu.xml', null, 'item');

        $this->setParameter('cms.menu.data', $parser->parse($this->getResourcePath().'/menu.xml'));
    }

    protected function prepareExtensions()
    {

    }

    /**
     * setupRoutes
     *
     * @param BuilderInterface $containerBuilder
     * @param mixed $uri
     *
     * @access private
     * @return mixed
     */
    private function setupRoutes(BuilderInterface $containerBuilder, $uri)
    {
        $routes = $containerBuilder->getContainer()->get('routing.routes');
        $childRoutes = new RouteCollection;


        $loader = $this->getRoutingLoader($containerBuilder, $routes);

        $loader->load(function ($routes) use ($uri, $containerBuilder) {
            $this->nestRoutes($routes, $containerBuilder, $uri);
        });

        $loader->load(function ($routes) {
            $routes->get('pages', '/{id?}', 'cms:front:index')->setConstraint('id', '.*?');
        });
    }

    /**
     * nestRoutes
     *
     * @param RouteBuilder $routes
     * @param BuilderInterface $builder
     * @param string $uri
     *
     * @return void
     */
    private function nestRoutes(RouteBuilder $routes, BuilderInterface $builder, $uri)
    {
        $loader = $this->getRoutingLoader($builder, $rc = new RouteCollection);
        $loader->load('routing.xml');

        $routes->group($uri, $this->getRouteRequirements());
        $routes->appendRoutes($rc);
        $routes->endGroup();

        foreach (['admin.login.index', 'admin.login.create'] as $name) {
            if (!$route = $routes->getRoutes()->get($name)) {
                continue;
            }

            $before = $route->getBeforeFilters();

            foreach ($before as $i => &$filter) {
                if ('auth' === $filter) {
                    unset($before[$i]);
                }
            }

            $route->setBeforeFilters($before);
        }
    }

    protected function getRouteRequirements()
    {
        return [
            'before' => 'auth'
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getResources()
    {
        return ['services.xml'];
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigTree(RootNode $rootNode)
    {
        $alias = $this->getPackageAlias();
        $rootNode
            ->string('uri')
                ->defaultValue('admin')
                ->notEmpty()
                ->end()
            ->string('extension_dir')
                ->optional()
                ->defaultValue('%app.root%/../extension')
                ->notEmpty()
                ->end();
    }

    private function setExtensionNamespaces(ContainerInterface $container, Parser $parser)
    {
        $ns = [];
        $extensions = [];
        $autoloads = [];

        foreach ($parser->readfiles() as $file => $info) {
            list ($namespace, $extension) = $info;

            $ns[$file] = $namespace;
            $extensions[] = $extension;

            $this->findAutoload($file, $autoloads);
        }

        $container->setParameter('cms.extension.namespaces', $ns);
        $container->setParameter('cms.extension.classes', $extensions);
        $container->setParameter('cms.extension.autoload', $autoloads);
    }

    private function findAutoload($file, array &$autoloads)
    {
        if (!$composer = $this->findComposer($file)) {
            return;
        }

        $cmp = json_decode(file_get_contents($composer), true);

        if (!isset($cmp['autoload'])) {
            return;
        }

        foreach ($cmp['autoload'] as $key => $load) {
            $kk = $load[$k = key($load)];
            $cmp['autoload'][$key][$k] = dirname($composer) . (strlen($kk) ? DIRECTORY_SEPARATOR . $kk : '');
        }

        $autoloads = array_merge_recursive($autoloads, $cmp['autoload']);
    }

    private function findComposer($file)
    {
        $f = $file;
        $t = 4;
        $found = false;

        do {
            if (!is_file($f = dirname($f).DIRECTORY_SEPARATOR.'composer.json')) {
                $f = dirname($f);
            } else {
                $found = true;
            }

            $t--;

        } while ($t > 0 && !$found);

        if ($found) {
            return $f;
        }
    }
}
