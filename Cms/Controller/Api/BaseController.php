<?php

/*
 * This File is part of the Selene\Package\Cms package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Controller\Api;

use \Selene\Module\Routing\Controller\Controller;
use \Selene\Package\Cms\Api\Formatter\FormatterInterface;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\AcceptHeader;
use \Symfony\Component\HttpFoundation\AcceptHeaderItem;
use \League\Fractal\TransformerAbstract;

/**
 * @class BaseController
 * @package Selene\Package\Cms
 * @version $Id$
 */
abstract class BaseController extends Controller implements ControllerInterface
{
    /**
     * formatter
     *
     * @var FormatterInterface
     */
    private $formatter;

    /**
     * transformer
     *
     * @var void
     */
    private $transformer;

    /**
     * setTransformer
     *
     * @param mixed $transformer
     *
     * @return void
     */
    public function setTransformer(TransformerAbstract $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * setFormatter
     *
     * @param FormatterInterface $formatter
     *
     * @return viod
     */
    public function setFormatter(FormatterInterface $formatter)
    {
        $this->formatter = $formatter;
    }

    /**
     * getDataMap
     *
     * @return array
     */
    abstract public function getDataMap();

    /**
     * response
     *
     * @param Request $request
     * @param mixed $data
     * @param int $status
     *
     * @return Response
     */
    protected function response(Request $request, $data, $status = 200)
    {
        if (null !== ($item = $this->findFormat($request))) {
            $accept = new AcceptHeader([$item]);
        } else {
            $accept = AcceptHeader::fromString($request->headers->get('Accept'));
        }

        $formatter = $this->getFormatter();

        foreach ($accept->all() as $format) {
            if ($formatter->supports($type = $format->getValue())) {
                return $this->sendResponse($formatter->format($data), $type, $status);
            }
        }

        throw new \InvalidArgumentException(implode(',', $formats));
    }

    /**
     * notFound
     *
     * @param Request $request
     * @param mixed $data
     *
     * @return Response
     */
    protected function notFound(Request $request, $data)
    {
        return $this->response($request, $data, Response::HTTP_NOT_FOUND);
    }

    /**
     * error
     *
     * @param Request $request
     * @param mixed $error
     *
     * @return Response
     */
    protected function error(Request $request, $error)
    {
        return $this->response($request, $error, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * mapFormats
     *
     * @param string $format
     *
     * @return string|null
     */
    protected function mapFormats($format)
    {
        $formats = [
            'jsonp' => 'application/javascript',
            'js'    => 'application/javascript',
            'json'  => 'application/json',
            'xml'   => 'application/xml',
        ];

        if (in_array($format, array_keys($formats))) {
            return $formats[$format];
        }
    }

    /**
     * findFormat
     *
     * @param Request $request
     *
     * @return AcceptHeaderItem|null
     */
    protected function findFormat(Request $request)
    {
        if (null !== ($format = $request->query->get('format'))) {
            return new AcceptHeaderItem(strtolower($format));
        }
    }

    /**
     * sendResponse
     *
     * @param mixed $data
     * @param mixed $type
     * @param int $status
     *
     * @return Response
     */
    protected function sendResponse($data, $type, $status = 200)
    {
        $resp = new Response($data, $status);

        $resp->headers->set('Content-type', $type);

        return $resp;
    }

    /**
     * getTransformer
     *
     * @return Transformer
     */
    protected function getTransformer()
    {
        return $this->transformer;
    }

    /**
     * getFormatter
     *
     * @return FormatterInterface
     */
    protected function getFormatter()
    {
        return $this->formatter;
    }
}
