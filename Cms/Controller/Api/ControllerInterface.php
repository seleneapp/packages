<?php

/*
 * This File is part of the Selene\Package\Cms package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Controller\Api;

use \League\Fractal\TransformerAbstract;
use \Selene\Package\Cms\Api\Data\Mapable;

/**
 * @interface ControllerInterface
 * @package Selene\Package\Cms
 * @version $Id$
 */
interface ControllerInterface extends Mapable
{
    /**
     * setTransformer
     *
     * @param mixed $transformer
     *
     * @return void
     */
    public function setTransformer(TransformerAbstract $transformer);

    /**
     * getDataMap
     *
     * @return array
     */
    public function getDataMap();
}
