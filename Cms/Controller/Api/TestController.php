<?php

/*
 * This File is part of the Selene\Package\Cms\Controller\Api package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Controller\Api;

use \Symfony\Component\HttpFoundation\Request;

/**
 * @class TestController
 * @package Selene\Package\Cms\Controller\Api
 * @version $Id$
 */
class TestController extends BaseController
{
    public function getDataAction(Request $request = null)
    {
        return $this->response($request, [['foo' => 'bar']], 200);
    }

    public function getDataMap()
    {
        return [];
    }
}
