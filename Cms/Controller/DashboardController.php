<?php

/*
 * This File is part of the Selene\Package\Cms\Controller package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Controller;

use \Symfony\Component\HttpFoundation\Request;

/**
 * @class DashboardController
 * @package Selene\Package\Cms\Controller
 * @version $Id$
 */
class DashboardController extends AdminController
{
    public function showAction(Request $request)
    {
        $this->ok($this->render('cms:content.dashboard:index.twig', ['dashboard' => 'heres the dashboard']));
    }
}
