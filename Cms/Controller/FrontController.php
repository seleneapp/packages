<?php

/*
 * This File is part of the Selene\Extension\Pages\Controller package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Controller;

use \Selene\Module\Routing\Controller\Controller;

/**
 * @class FrontController
 * @package Selene\Extension\Pages\Controller
 * @version $Id$
 */
class FrontController extends Controller
{
    public function indexAction($id = null)
    {
        var_dump($id);
        die;
    }

    public function callAction($method, array $parameters = [])
    {
        $parts = preg_split('~/~', $parameters['id'], -1, PREG_SPLIT_NO_EMPTY);

        var_dump($parts);

        return parent::callAction($method, $parameters);
    }
}
