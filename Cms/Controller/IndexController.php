<?php

/*
 * This File is part of the Selene\Package\Cms\Controller package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Controller;

/**
 * @class IndexController
 * @package Selene\Package\Cms\Controller
 * @version $Id$
 */
class IndexController extends AdminController
{
    /**
     * indexAction
     *
     * @return void
     */
    public function showAction()
    {
        $this->ok($this->render('cms:content:index.twig', ['admin' => 'test']));
    }

}
