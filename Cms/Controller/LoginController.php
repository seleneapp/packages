<?php

/*
 * This File is part of the Selene\Package\Cms package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Controller;

use \Symfony\Component\HttpFoundation\Request;

/**
 * @class LoginController
 * @package Selene\Package\Cms
 *
 * @version $Id$
 */
class LoginController extends AdminController
{
    /**
     * We will just render the login form
     */
    public function showAction(Request $request)
    {
        $this->ok($this->render('cms:login:form.twig'));
    }

    public function createAction(Request $request)
    {

    }
}
