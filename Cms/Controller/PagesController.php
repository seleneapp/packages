<?php

/*
 * This File is part of the Selene\Package\Cms\Controller package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Controller;

/**
 * @class PagesController
 * @package Selene\Package\Cms\Controller
 * @version $Id$
 */
class PagesController extends AdminController
{
    public function indexAction()
    {
        $this->ok($this->render('cms:content.pages:index.twig'));
    }

    public function createAction()
    {
        $this->ok($this->render('cms:content.pages:new.twig'));
    }
}
