<?php

/*
 * This File is part of the Selene\Package\Cms\Extension package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Extension;

use \Selene\Module\Package\PackageInterface;
use \Selene\Module\Package\ConfigLoader as BaseConfigLoader;

/**
 * @class myClass
 * @package Selene\Package\Cms\Extension
 * @version $Id$
 */
class ConfigLoader extends BaseConfigLoader
{
    public function addLoaded(PackageInterface $package)
    {
        //$this->addToLoaded($package->getAlias(), $package);
    }
}
