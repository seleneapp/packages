<?php

/*
 * This File is part of the Selene\Package\Cms\Extension package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Extension;

use \Selene\Module\Di\BuilderInterface;
use \Selene\Module\Package\PackageConfiguration;
use \Selene\Package\Cms\Extension\ExtensionInterface;

/**
 * @class Configuration
 * @package Selene\Package\Cms\Extension
 * @version $Id$
 */
class Configuration extends PackageConfiguration
{
    /**
     * Constructor.
     *
     * Don't allow arbitrary packages to register.
     *
     * @param ExtensionInterface $extension
     */
    public function __construct(ExtensionInterface $extension)
    {
        parent::__construct($extension);
    }

    /**
     * {@inheritdoc}
     */
    public function setup(BuilderInterface $builder, array $config)
    {
        $this->registerMenus($builder, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function load(BuilderInterface $builder, array $values)
    {
        $loader = $this->getRoutingLoader($builder, $routes = $builder->getContainer()->get('routing.routes'));

        foreach ($this->globResources('routing', ['xml','php'], true) as $file) {
            $loader->load($file);
        }

        parent::load($builder, $values);
    }

    /**
     * globResources
     *
     * @param mixed $name
     * @param array $extensions
     * @param bool  $basename
     *
     * @return array
     */
    protected function globResources($name, array $extensions = ['xml'], $basename = false)
    {
        if (empty($extensions)) {
            return $extensions;
        }

        $files = glob($this->getResourcePath() . '/'.$name.'.{'.implode(',', $extensions).'}', GLOB_BRACE) ?: [];

        if ($basename) {
            return array_map('basename', $files);
        }

        return $files;
    }

    /**
     * {@inheritdoc}
     */
    protected function getResources()
    {
        return $this->globResources('services', ['xml'], true);
    }

    /**
     * registerMenus
     *
     * @param BuilderInterface $builder
     * @param mixed $config
     *
     * @return void
     */
    private function registerMenus(BuilderInterface $builder, $config)
    {

    }
}
