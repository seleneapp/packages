<?php

/*
 * This File is part of the Selene\Package\Cms\Extension\Exception package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Extension\Exception;

/**
 * @class myClass
 * @package Selene\Package\Cms\Extension\Exception
 * @version $Id$
 */
class ExtensionLoadException extends \Exception
{
}
