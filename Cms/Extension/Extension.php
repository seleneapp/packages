<?php

/**
 * This File is part of the Selene\Package\Cms package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Extension;

use \Selene\Module\Package\Package;
use \Selene\Module\DI\BuilderInterface;
use \Selene\Module\Common\Helper\StringHelper;
use \Selene\Module\Package\PackageRepositoryInterface;
use \Selene\Adapter\Kernel\ApplicationInterface;
use \Selene\Adapter\Console\Application as Console;

/**
 * @abstract class Extension extends Package implements ExtensionInterface
 * @see ExtensionInterface
 * @see Package
 *
 * @package Selene\Package\Cms
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
abstract class Extension extends Package implements ExtensionInterface
{
    private $doesBoot;
    private $repository;

    /**
     * Constructor.
     *
     * @param Repository $repo
     */
    final public function __construct(PackageRepositoryInterface $repo)
    {
        $this->doesBoot = true;
        $this->repository = $repo;
    }

    /**
     * disableBoot
     *
     * @return void
     */
    final public function disableBoot()
    {
        $this->doesBoot = false;
    }

    /**
     * doesBoot
     *
     * @return boolean
     */
    final public function doesBoot()
    {
        return $this->doesBoot;
    }

    /**
     * Force the alias to start with an 'ext_' prefix.
     *
     * {@inheritdoc}
     */
    final public function getAlias()
    {
        if (null === $this->alias || 0 !== strpos($this->alias, 'ext_')) {
            $this->alias = sprintf('ext_%s', $this->getAliasPostFix() ?: $this->getRealAliasName());
        }

        return $this->alias;
    }

    /**
     * getType
     *
     * @return int
     */
    public function getType()
    {
        return self::T_BACKEND|self::T_FRONTEND;
    }

    /**
     * {@inheritdoc}
     */
    public function build(BuilderInterface $builder)
    {
    }

    /**
     * {@inheritdoc}
     */
    final public function requires()
    {
        return array_merge(['cms'], $this->getRequirements());
    }

    /**
     * isEnabled
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->repository->isEnabled($this);
    }

    /**
     * isInstalled
     *
     * @return boolean
     */
    public function isInstalled()
    {
        return $this->repository->isInstalled($this);
    }

    /**
     * enable
     *
     * @return void
     */
    public function enable()
    {
        $this->repository->enable($this);
    }

    /**
     * install
     *
     * @return void
     */
    public function install()
    {
        $this->repository->install($this);
    }

    /**
     * tearDown
     *
     * @return void
     */
    public function tearDown()
    {
    }

    /**
     * getRequirements
     *
     * @return array
     */
    protected function getRequirements()
    {
        return [];
    }

    /**
     * getAliasPostfix
     *
     * @return string
     */
    protected function getAliasPostfix()
    {
        return $this->getRealAliasName();
    }

    /**
     * getRealAliasName
     *
     * @return string
     */
    final protected function getRealAliasName()
    {
        $name = $this->getName();
        $base = 0 !== strripos($name, 'extension') ? substr($name, 0, -9) : $name;

        return StringHelper::strLowDash($base);
    }
}
