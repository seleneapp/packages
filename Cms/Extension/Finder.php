<?php

/*
 * This File is part of the Selene\Package\Cms\Extension package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Extension;

use \FilesystemIterator;
use \Selene\Module\Filesystem\PatternIterator;

/**
 * @class Finder
 *
 * @package Selene\Package\Cms\Extension
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Finder
{
    const EXT_PTT = '([\w+])+(Extension\.php)$';

    private $path;
    private $pattern;
    private $depth;

    /**
     * Constructor.
     *
     * @param mixed $path
     * @param mixed $pattern
     * @param int $depth
     */
    public function __construct($path, $pattern = null, $depth = 4)
    {
        $this->path    = $path;
        $this->pattern = $this->getPattern($pattern);
        $this->depth   = (int)$depth;
    }

    /**
     * find
     *
     * @param callable $callback
     *
     * @return array
     */
    public function find(callable $callback = null)
    {
        $results = [];

        $flags = FilesystemIterator::FOLLOW_SYMLINKS|\FilesystemIterator::SKIP_DOTS;

        foreach ($itr = new FilesystemIterator($this->path) as $item) {

            if (!$item->isDir() || (null !== $callback && false !== call_user_func($callback, $itr))) {
                continue;
            }

            foreach ($this->getIterator($item->getRealPath(), $this->pattern, $this->depth, $flags) as $match) {
                $results[] = $match->getRealPath();
            }
        }

        return $results;
    }

    /**
     * Get the inner Iterrator.
     *
     * Returns the inner instance of the PatternIterator.
     *
     * @param string $path
     * @param string $pattern
     * @param int    $depth
     * @param int    $flags
     *
     * @return PatternIterator
     */
    protected function getIterator($path, $pattern, $depth, $flags)
    {
        return new PatternIterator($path, $pattern, $depth, PatternIterator::MATCH, $flags);
    }

    /**
     * getPattern
     *
     * @param string $pattern
     *
     * @return string
     */
    protected function getPattern($pattern = null)
    {
        return sprintf('~%s~', $pattern ?: self::EXT_PTT);
    }
}
