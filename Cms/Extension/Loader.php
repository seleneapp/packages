<?php


/*
 * This File is part of the Selene\Package\Cms\Extension package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Extension;

use \Composer\Autoload\ClassLoader;
use \Selene\Module\Package\PackageRepositoryInterface;

/**
 * @class myClass
 * @package Selene\Package\Cms\Extension
 * @version $Id$
 */
class Loader
{
    private $loader;

    private $classes;

    private $autoload;

    private $repository;

    /**
     * Constructor.
     *
     * @param Repository $repo
     * @param ClassLoader $loader
     * @param array $classes
     * @param array $loadMap
     */
    public function __construct(PackageRepositoryInterface $repo, ClassLoader $loader, array $classes, array $loadMap)
    {
        $this->repository   = $repo;
        //$this->loader       = new ClassLoader;
        $this->loader       = $loader;
        $this->classes      = $classes;
        $this->autoload     = $loadMap;
    }

    /**
     * loadExtensions
     *
     * @return void
     */
    public function load()
    {
        foreach ($this->classes as $class) {

            $this->loader->unregister();

            if (!class_exists($class)) {
                $this->autoLoadClass($class);
            }

            $this->loader->register();

            try {
                $this->repository->add(new $class($this->repository));
            } catch (\Exception $e) {
                throw new ExtensionLoadException($e->getMessage());
            }
        }
    }

    /**
     * autoloadClass
     *
     * @param string $Class
     *
     * @return void
     */
    private function autoloadClass($Class)
    {
        $ns = rtrim(substr($Class, 0, strrpos($Class, '\\')), '\\').'\\';

        //var_dump(class_exists($Class));
        foreach (['psr-0', 'psr-4'] as $type) {

            if (!isset($this->autoload[$type][$ns])) {
                continue;
            }

            $path = $this->autoload[$type][$ns];

            $file = $path.DIRECTORY_SEPARATOR.substr($Class, strlen($ns)).'.php';

            //$this->loader->setPsr4($ns, $path);
            //$this->loader->addClassMap([$Class => $file]);

            if ('psr-0' === $type) {
                $this->loader->set($ns, $path);
            } else {
                $this->loader->setPsr4($ns, $path);
            }
        }
    }
}
