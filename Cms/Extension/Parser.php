<?php

/*
 * This File is part of the Selene\Package\Cms\Extension package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Extension;

use \Composer\Autoload\ClassLoader;
use \Selene\Module\Filesystem\Filereader;

/**
 * @class myClass
 * @package Selene\Package\Cms
 * @version $Id$
 */
class Parser
{
    private $finder;
    private $loader;
    private $extensionInterface = 'Selene\Package\Cms\Extension\ExtensionInterface';

    /**
     * Constructor
     *
     * @param Finder $finder
     * @param ClassLoader $loader
     *
     * @access public
     * @return mixed
     */
    public function __construct(Finder $finder, ClassLoader $loader = null)
    {
        $this->finder = $finder;
        $this->loader = $loader;
    }

    /**
     * setExtensionInterface
     *
     * @param mixed $interface
     *
     * @return void
     */
    public function setExtensionInterface($interface)
    {
        $this->extensionInterface = $interface;
    }

    /**
     * getExtensionInterface
     *
     * @return string
     */
    public function getExtensionInterface()
    {
        return $this->extensionInterface;
    }

    public function getNamespaces(array $map)
    {
        $ns = [];

        foreach ($map as $file => $info) {
            list($namespace, $className) = $info;
            $ns[$namespace] = $file;
        }

        return $ns;
    }

    /**
     * readfiles
     *
     * @return void
     */
    public function readfiles()
    {
        $map = [];

        foreach ($this->finder->find() as $file) {

            $ns     = null;
            $class  = null;
            (new FileReader($file))->read(function ($line) use (&$ns, &$class) {
                if (0 === stripos($line, 'namespace ')) {
                    list ($ns) = explode(' ', substr(trim($line), 10, -1), 2);
                } elseif (0 === stripos($line, 'class ')) {
                    list ($class) = explode(' ', trim(substr($line, 5)), 2);

                    return true;
                }
            });

            if (null !== $class && $this->analizeClass($ns, $className = $this->getClassName($ns, $class), $file)) {
                $map[$file] = [$ns, $className];
            }
        }

        return $map;
    }

    private function getClassName($ns, $class)
    {
        return rtrim(sprintf('%s\%s', $ns, $class), '\\');
    }

    /**
     * analizeClass
     *
     * @param mixed $ns
     * @param mixed $class
     *
     * @return void
     */
    private function analizeClass($ns, $className, $file)
    {
        return true;

        try {
            $rf = new \ReflectionClass($className);
        } catch (\ReflectionException $e) {
            return false;
        }

        return $rf->implementsInterface($this->getExtensionInterface());
    }
}
