<?php

/**
 * This File is part of the Selene\Package\Cms package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Extension;

use \Selene\Module\DI\BuilderInterface;
use \Selene\Module\Events\DispatcherInterface;
use \Selene\Module\Package\PackageInterface;
use \Selene\Module\Package\PackageRepository;
use \Selene\Module\Package\PackageRepositoryInterface;
use \Selene\Adapter\Kernel\ApplicationInterface;
use \Composer\Autoload\ClassLoader;

/**
 * @class Repository Repository
 *
 * @package Selene\Package\Cms
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Repository extends PackageRepository
{
    /**
     * repo
     *
     * @var PackageRepository
     */
    private $repo;
    private $building;
    private $extensions;

    /**
     * Construct.
     *
     * @param array $extensions
     * @param PackageRepositoryInterface $repo building reference
     */
    public function __construct(array $extensions = [], PackageRepositoryInterface $repo = null)
    {
        parent::__construct($extensions, new ConfigLoader);
        $this->repo = $repo ?: new PackageRepository;
        $this->building = false;
    }

    /**
     * addExtensions
     *
     * @param array $extensions
     *
     * @return void
     */
    public function addExtensions(array $extensions)
    {
        return $this->addPackages($extensions);
    }

    /**
     * {@inheritdoc}
     */
    public function build(BuilderInterface $builder)
    {
        $this->extensions = array_keys($this->packages);

        if ($this->repo->wasBuilt()) {
            foreach ($this->repo->all() as $package) {
                $this->configLoader->load($builder, $package);
            }
        }

        $this->building = true;

        parent::build($builder);

        // reset packeges;
        $this->packages = array_filter($this->packages, function ($package) {
            return in_array($package->getAlias(), $this->extensions);
        });

        $this->sorted = false;
        $this->building = false;
    }

    /**
     * {@inheritdoc}
     */
    protected function isBuildable(PackageInterface $package)
    {
        if (!($isExt = $package instanceof Extension)) {
            //$this->configLoader->addLoaded($package);
        }

        return in_array($package->getAlias(), $this->extensions) &&
            $isExt && parent::isBuildable($package);
    }

    /**
     * addExtension
     *
     * @param ExtensionInterface $ext
     *
     * @return void
     */
    public function addExtension(ExtensionInterface $ext)
    {
        if ($this->hasExtension($name = $ext->getAlias())) {
            throw new \InvalidArgumentException(
                sprintf('Extension "%s" already registered or duplicate extension name.', $name)
            );
        }

        return $this->addPackage($ext);
    }

    public function uninstall(ExtensionInterface $ext)
    {
        // do stuff
        $ext->tearDown();
    }

    public function install(ExtensionInterface $ext)
    {
    }

    public function enable(ExtensionInterface $ext)
    {
    }

    public function disable(ExtensionInterface $ext)
    {
    }

    public function getRepository()
    {
        return $this->repo;
    }

    public function isBackendExtension(Extension $ext)
    {
        return Extension::T_BACKEND === (Extension::T_BACKEND & $ext->getType());
    }

    /**
     * boot
     *
     * @param ApplicationInterface $app
     *
     * @return void
     */
    public function boot(ApplicationInterface $app)
    {
        foreach ((array)$this->packages as $extension) {
            if ($this->isEnabled($extension) && $this->isBootable($extension)) {
                $extension->boot($app);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function has($key)
    {
        if (!$this->building) {
            return parent::has($key);
        }

        return parent::has($key) || $this->repo->has($key);
    }

    /**
     * {@inheritdoc}
     */
    public function get($name)
    {
        if ($this->building && $this->repo->has($name)) {
            return $this->repo->get($name);
        }

        return parent::get($name);
    }

    protected function isInstalled(ExtensionInterface $ext)
    {
        return true;
    }

    protected function isEnabled(ExtensionInterface $ext)
    {
        return true;
    }

    protected function isBootable(ExtensionInterface $ext)
    {
        return $ext->doesBoot();
    }

    /**
     * getSorted
     *
     * @return array
     */
    protected function getSorted()
    {
        if (!$this->sorted) {
            $this->packages = array_merge($this->repo->all(), $this->packages);
        }

        return parent::getSorted();
    }
}
