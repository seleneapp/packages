<?php

/**
 * This File is part of the Selene\Package\Cms\Filesystem package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Filesystem;

use \Selene\Module\Filesystem\StreamWrapper;

/**
 * @class TemplateStream
 * @package Selene\Package\Cms\Filesystem
 * @version $Id$
 */
class ResourceStream extends StreamWrapper
{
    /**
     * instantiator
     *
     * @var Closure
     */
    private static $instantiator;

    /**
     * map
     *
     * @var array
     */
    private $map;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->setMap();
    }

    /**
     * setMapInstantiator
     *
     * @param \Closure $context
     *
     * @return void
     */
    public static function setMapInstantiator(\Closure $context)
    {
        static::$instantiator =& $context;
    }

    /**
     * Get the local target path from the protocol string.
     *
     * @param string $uri
     *
     * @return string
     */
    protected function getTarget($uri)
    {
        list(, $target) = explode('://', $uri, 2);

        if (!$target = $this->parseUri($target)) {
            return $uri;
        }

        return $target;
    }

    /**
     * Parse the protocl path to an actual file path.
     *
     * @param string $uri
     *
     * @return string|bool false if resource is not set.
     */
    protected function parseUri($uri)
    {
        $map = $this->getMap();

        list ($package, $resource, $path) = explode(':', $uri);

        if (!isset($map[$package][$resource])) {
            return false;
        }

        return sprintf('%1$s%3$s%2$s', $map[$package][$resource], $path, DIRECTORY_SEPARATOR);
    }

    /**
     * getMap
     *
     * @return array
     */
    protected function getMap()
    {
        return $this->map ?: [];
    }

    /**
     * setMap
     *
     * @return void
     */
    private function setMap()
    {
        if (null === $this->map && is_callable(static::$instantiator)) {
            $this->map = call_user_func(static::$instantiator);
        }
    }

}
