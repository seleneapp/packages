<?php

/*
 * This File is part of the Selene\Package\Cms\Menu package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Menu;

use \Selene\Module\Common\Data\Collection;

/**
 * @class Attributes
 * @package Selene\Package\Cms\Menu
 * @version $Id$
 */
class Attributes extends Collection
{
    public static function fromArray(array $data)
    {
        $attrs = new static;
        $attrs->initialize($data);

        return $attrs;
    }
}
