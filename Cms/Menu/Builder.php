<?php

/*
 * This File is part of the Selene\Package\Cms package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Menu;

/**
 * @class Builder
 * @package Selene\Package\Cms
 * @version $Id$
 */
class Builder
{
    /**
     * root
     *
     * @var Node
     */
    private $root;

    /**
     * nodes
     *
     * @var array
     */
    private $nodes;

    /**
     * Constructor;
     *
     * @param string $root
     */
    public function __construct($root = 'root')
    {
        $this->nodes = [];
        $this->setRootNode($root);
    }

    /**
     * addNode
     *
     * @param string $name
     * @param string $parent
     *
     * @return Node|null
     */
    public function addNode($name, $parent = null)
    {
        if ($parentNode = $this->getParentNode($parent)) {
            return $this->setNode($parentNode, $name);
        }
    }

    /**
     * addNodes
     *
     * @param string $nodes
     * @param string $parent
     *
     * @return void
     */
    public function addNodes($nodes, $parent = null)
    {
        if (!$parentNode = $this->getParentNode($parent)) {
            return;
        }

        foreach ($nodes as $name => $child) {
            if (is_string($child)) {
                $this->setNode($parentNode, $child);
            }
        }
    }

    /**
     * getNodes
     *
     * @return array
     */
    public function getNodes()
    {
        return $this->nodes;
    }

    /**
     * getTree
     *
     * @return Node
     */
    public function getTree()
    {
        return $this->root;
    }

    /**
     * find
     *
     * @param string $name
     *
     * @return Node|null
     */
    public function find($name)
    {
        if (isset($this->nodes[$name])) {
            return $this->nodes[$name];
        }

        return $this->findNode($this->root, $name);
    }

    /**
     * fromArray
     *
     * @param array $tree
     * @param string $root
     *
     * @return Builder
     */
    public static function fromArray(array $tree = [], $root = 'root')
    {
        $nodes = isset($tree[$root]) ? $tree[$root] : $tree;
        $builder = new static($root);

        foreach ($nodes as $name => $child) {
            if (is_string($child)) {
                $builder->addNode($child, $root);
            } elseif (is_array($child) && is_string($name)) {
                if ($parent = $builder->addNode($name, $root)) {
                    $builder->addNodes($child, $parent->getName());
                }
            }
        }

        return $builder;
    }

    /**
     * setRootNode
     *
     * @param string $root
     *
     * @return void
     */
    protected function setRootNode($root)
    {
        $this->root = new Node($root);
        $this->nodes[$root] = &$this->root;
    }

    /**
     * getParentNode
     *
     * @param string $parent
     *
     * @return Node
     */
    protected function getParentNode($parent = null)
    {
        return null !== $parent ? $this->find($parent) : $this->root;
    }

    /**
     * setNode
     *
     * @param Node $parent
     * @param string $name
     *
     * @return Node
     */
    protected function setNode(Node $parent, $name)
    {
        $child = new Node($name, $parent->getName());
        $parent->addChild($child);

        if (!is_string($name)) {
            throw new \InvalidArgumentException('Name must be string');
        }

        $this->nodes[$name] = &$child;

        return $child;
    }

    /**
     * findNode
     *
     * @param Node $parent
     * @param string $nodeName
     *
     * @return Node
     */
    protected function findNode(Node $parent, $nodeName)
    {
        foreach ($parent->getChildren() as $child) {
            if ($nodeName === $child->getName()) {
                return $child;
            } elseif ($node = $this->findNode($child, $nodeName)) {
                return $node;
            }
        }
    }

}
