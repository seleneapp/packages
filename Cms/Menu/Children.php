<?php

/*
 * This File is part of the Selene\Package\Cms\Menu package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Menu;

/**
 * @class Children
 * @package Selene\Package\Cms\Menu
 * @version $Id$
 */
class Children implements \Countable
{
    private $nodes;
    private $sorted;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->nodes  = [];
        $this->sorted = false;
    }

    /**
     * add
     *
     * @param NodeInterface $node
     *
     * @return void
     */
    public function add(NodeInterface $node)
    {
        $node->setPosition($this->count());

        $this->nodes[$hash = spl_object_hash($node)] =& $node;

        $this->sorted = false;
    }

    /**
     * remove
     *
     * @param NodeInterface $node
     *
     * @return void
     */
    public function remove(NodeInterface $node)
    {
        if ($this->has($node)) {

            unset($this->nodes[spl_object_hash($node)]);

            $this->sorted = false;
        }
    }

    /**
     * has
     *
     * @param NodeInterface $node
     *
     * @return boolean
     */
    public function has(NodeInterface $node)
    {
        return isset($this->nodes[spl_object_hash($node)]);
    }

    /**
     * count
     *
     * @return int
     */
    public function count()
    {
        return count($this->nodes);
    }

    /**
     * all
     *
     * @return array
     */
    public function all()
    {
        $this->sort();

        return $this->nodes;
    }

    /**
     * findByName
     *
     * @param mixed $name
     *
     * @return NodeInterface|null
     */
    public function findByName($name)
    {
        foreach ($this->nodes as $node) {
            if ($name === $node->getName()) {
                return $node;
            }
        }
    }

    /**
     * removeByName
     *
     * @param mixed $name
     *
     * @return boolean
     */
    public function removeByName($name)
    {
        if ($node = $this->findByName($name)) {
            $this->remove($node);

            return true;
        }

        return false;
    }

    /**
     * sort
     *
     * @return void
     */
    private function sort()
    {
        if ($this->sorted) {
            return;
        }

        uasort($this->nodes, function ($a, $b) {
            return $a->getPosition() > $b->getPosition() ? 1 : -1;
        });

        $this->sorted = true;
    }
}
