<?php

/*
 * This File is part of the Selene\Package\Cms\Menu package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Menu;

use \Selene\Module\Xml\Parser;
use \Selene\Module\Xml\Loader\Loader;
use \Selene\Module\Xml\Loader\LoaderInterface;

/**
 * @class ConfigParser
 * @package Selene\Package\Cms\Menu
 * @version $Id$
 */
class ConfigParser
{
    public function __construct($file, LoaderInterface $loader = null, $itemKey = 'item')
    {
        $this->file = $file;
        $this->loader = $loader ?: new Loader;
        $this->itemKey = 'item';
    }

    public function setMenuItemKey($key)
    {
        $this->itemKey = $key;
    }

    public function getMenuItemKey()
    {
        return $this->itemKey;
    }

    public function parse()
    {
        $dom = $this->loader->load($this->file);

        $tree = [];

        $this->parseNodes($dom->xpath($this->getItemXpath()), $tree);

        return $tree;
    }

    protected function parseNodes(\DOMNodeList $nodes, array &$tree = [])
    {
        foreach ($nodes as $item) {

            if ($this->getMenuItemKey() !== $item->nodeName && 'root' !== $item->nodeName) {
                continue;
            }

            $childNodes = $item->xpath($this->getItemXpath());

            if (0 < $childNodes->length) {
                $name = (string)$item->getAttribute('name');
                $tree[$name] = [];
                $this->parseNodes($item->childNodes, $tree[$name]);
            } else {
                $tree[] = (string)$item->getAttribute('name');
            }
        };
    }

    protected function getItemXpath()
    {
        return './'.$this->getMenuItemKey();
    }
}
