<?php

/*
 * This File is part of the Selene\Package\Cms\Menu package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Menu;

/**
 * @class Factory
 * @package Selene\Package\Cms\Menu
 * @version $Id$
 */
class Factory
{
    public static function make($class, $tree)
    {
        return Builder::fromArray($tree, 'admin.index');
    }
}
