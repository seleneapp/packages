<?php

/*
 * This File is part of the Selene\Package\Cms package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Menu;

use \Selene\Module\Routing\UrlBuilder;
use \Selene\Module\Routing\RouterInterface;

/**
 * @class Menu implements \IteratorAggregate
 * @see \IteratorAggregate
 *
 * @package Selene\Package\Cms
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Menu implements \IteratorAggregate
{
    /**
     * router
     *
     * @var RouterInterface
     */
    private $router;

    /**
     * rootNode
     *
     * @var NodeInterface
     */
    private $rootNode;

    /**
     * url
     *
     * @var UrlBuilder
     */
    private $url;

    /**
     * Constructor.
     *
     * @param NodeInterface   $root
     * @param RouterInterface $router
     * @param UrlBuilder      $url
     */
    public function __construct(NodeInterface $root, RouterInterface $router, UrlBuilder $url)
    {
        $this->rootNode = $root;
        $this->router   = $router;
        $this->url      = $url;
    }

    /**
     * items
     *
     * @return Children
     */
    public function items()
    {
        return $this->rootNode->getChildren();
    }

    /**
     * getRoot
     *
     * @return NodeInterface
     */
    public function getRoot()
    {
        return $this->rootNode;
    }

    /**
     * isActiive
     *
     * @param NodeInterface $node
     *
     * @return boolean
     */
    public function isCurrent(NodeInterface $node)
    {
        return $this->router->getCurrentRoute() === $node->getName();
    }

    /**
     * isActive
     *
     * @param NodeInterface $node
     *
     * @return boolean
     */
    public function isActive(NodeInterface $node)
    {
        return $this->isCurrent($node) || $this->hasCurrentChild($node, $this->router->getCurrentRoute());
    }

    /**
     * getUrl
     *
     * @param Node $node
     * @param boolean $relative
     *
     * @return string
     */
    public function getUrl(Node $node, array $attrs = [], $host = null, $relative = true)
    {
        return $this->url->getPath(
            $node->getName(),
            $attrs,
            $host,
            $relative ? UrlBuilder::RELATIVE_PATH : UrlBuilder::ABSOLUTE_PATH
        );
    }

    /**
     * getIterator
     *
     * @return Iterator
     */
    public function getIterator()
    {
        return $this->rootNode->getIterator();
    }

    /**
     * hasCurrentChild
     *
     * @param NodeInterface $node
     * @param string $current
     *
     * @return boolean
     */
    protected function hasCurrentChild(NodeInterface $node, $current)
    {
        foreach ($node as $child) {
            if ($current === $child->getName() || $this->hasCurrentChild($child, $current)) {
                return true;
            }
        }

        return false;
    }

}
