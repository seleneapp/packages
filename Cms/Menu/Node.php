<?php

/*
 * This File is part of the Selene\Package\Cms\Menu package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Menu;

use \Selene\Module\Common\Data\CollectionInterface;

/**
 * @class Node
 * @package Selene\Package\Cms\Menu
 * @version $Id$
 */
class Node implements NodeInterface, \IteratorAggregate
{
    private $name;
    private $parent;
    private $children;
    private $attributes;
    private $position;

    /**
     * Constructor.
     *
     * @param string     $name
     * @param string     $parent
     * @param attributes $attrbutes
     */
    public function __construct($name, $parent = null, array $attributes = [])
    {
        $this->name     = $name;
        $this->parent   = $parent;
        $this->position = 0;
        $this->attributesFromArray($attributes);

        $this->children = new Children;
    }

    /**
     * addAttribute
     *
     * @param mixed $key
     * @param mixed $value
     *
     * @return void
     */
    public function addAttribute($key, $value)
    {
        $this->attributes->add($key, $value);
    }

    /**
     * attributesFromArray
     *
     * @param array $attributes
     *
     * @return Attributes
     */
    public function attributesFromArray(array $attributes)
    {
        return $this->setAttributes(Attributes::fromArray($attributes));
    }

    /**
     * setAttributes
     *
     * @param Attributes $attrs
     *
     * @return void
     */
    public function setAttributes(CollectionInterface $attrs)
    {
        $this->attributes = $attrs;
    }

    /**
     * getAttribute
     *
     * @param mixed $key
     *
     * @return mixed
     */
    public function getAttribute($key)
    {
        return $this->attributes->get($key);
    }

    /**
     * getAttributes
     *
     * @return Attributes
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * possition
     *
     * @return void
     */
    public function setPosition($pos)
    {
        $this->position = (int)$pos;
    }

    /**
     * getPosition
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * getChildren
     *
     * @return Children
     */
    public function getChildren()
    {
        return $this->children->all();
    }

    /**
     * hasChildren
     *
     * @return boolean
     */
    public function hasChildren()
    {
        return 0 < $this->children->count();
    }

    /**
     * addChild
     *
     * @param Node $node
     *
     * @return void
     */
    public function addChild(Node $node)
    {
        $this->children->add($node);
    }

    /**
     * hasChild
     *
     * @param mixed $name
     *
     * @return boolean
     */
    public function hasChild($name)
    {
        return (bool)$this->getChild($name);
    }

    /**
     * getChild
     *
     * @param mixed $name
     *
     * @return Node|null
     */
    public function getChild($name)
    {
        foreach ($this->children as $child) {
            if ($name === $child->getName()) {
                return $child;
            }
        }
    }

    /**
     * getParent
     *
     * @return void
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * hasParent
     *
     * @return boolean
     */
    public function hasParent()
    {
        return null !== $this->getParent();
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->children->all());
    }

    /**
     * Magick attribute getter
     *
     * @param mixed $key
     *
     * @return mixed
     */
    public function __get($key)
    {
        return $this->getAttribute($key);
    }
}
