<?php

/*
 * This File is part of the Selene\Package\Cms package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Menu;

use \Selene\Module\Common\Data\CollectionInterface;

/**
 * @class NodeInterface
 * @package Selene\Package\Cms
 * @version $Id$
 */
interface NodeInterface
{
    /**
     * getName
     *
     * @param mixed $name
     *
     * @return string
     */
    public function getName();

    /**
     * getParent
     *
     * @param mixed $name
     *
     * @return string|null
     */
    public function getParent();

    /**
     * hasChildren
     *
     * @return bool
     */
    public function hasChildren();

    /**
     * getChildren
     *
     * @return Children
     */
    public function getChildren();

    /**
     * getAttribute
     *
     * @param mixed $key
     *
     * @return mixed
     */
    public function getAttribute($key);

    /**
     * getAttributes
     *
     * @return Attributes
     */
    public function getAttributes();

    /**
     * setAttributes
     *
     * @param CollectionInterface $attrs
     *
     * @return void
     */
    public function setAttributes(CollectionInterface $attrs);
}
