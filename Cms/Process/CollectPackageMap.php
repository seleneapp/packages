<?php

/**
 * This File is part of the Selene\Package\Cms\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Process;

use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Processor\ProcessInterface;

/**
 * @class CollectPackageMap
 * @package Selene\Package\Cms\Process
 * @version $Id$
 */
class CollectPackageMap implements ProcessInterface
{
    public function process(ContainerInterface $container)
    {
        $app = $container->get('app');

        $map = [];

        foreach ($app->getPackages() as $package) {
            $map[$package->getAlias()] = $this->getPackageInfo($package);
        }

        $container->setParameter('package.resources', $map);
    }

    private function getPackageInfo($package)
    {
        $path = $package->getResourcePath();

        $info = [];

        foreach (['view', 'assets', 'config'] as $res) {
            $rpath = $path . DIRECTORY_SEPARATOR . $res;

            if (is_dir($rpath)) {
                $info[$res] = $rpath;
            }
        }

        return $info;
    }
}
