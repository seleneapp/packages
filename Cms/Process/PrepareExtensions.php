<?php

/*
 * This File is part of the Selene\Package\Cms\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Process;

use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Processor\ProcessInterface;
use \Selene\Package\Cms\Extension\ExtensionInterface;
use \Selene\Package\Cms\Extension\Finder;
use \Selene\Package\Cms\Extension\Parser;

/**
 * @class myClass
 * @package Selene\Package\Cms\Process
 * @version $Id$
 */
class PrepareExtensions implements ProcessInterface
{
    public function process(ContainerInterface $container)
    {
        $this->setExtensionNamespaces(
            $container,
            new Parser(new Finder($path = $container->getParameter('cms.extension.dir')))
        );
    }

    private function setExtensionNamespaces(ContainerInterface $container, Parser $parser)
    {
        $ns = [];
        $extensions = [];
        $autoloads = [];

        foreach ($parser->readfiles() as $file => $info) {
            list ($namespace, $extension) = $info;

            $ns[$file] = $namespace;
            $extensions[] = $extension;

            $this->findAutoload($file, $autoloads);
        }

        $container->setParameter('cms.extension.namespaces', $ns);
        $container->setParameter('cms.extension.classes', $extensions);
        $container->setParameter('cms.extension.autoload', $autoloads);
    }

    private function findAutoload($file, array &$autoloads)
    {
        if (!$composer = $this->findComposer($file)) {
            return;
        }

        $cmp = json_decode(file_get_contents($composer), true);

        if (!isset($cmp['autoload'])) {
            return;
        }

        foreach ($cmp['autoload'] as $key => $load) {
            $kk = $load[$k = key($load)];
            $cmp['autoload'][$key][$k] = dirname($composer) . (strlen($kk) ? DIRECTORY_SEPARATOR . $kk : '');
        }

        $autoloads = array_merge_recursive($autoloads, $cmp['autoload']);
    }

    private function findComposer($file)
    {
        $f = $file;
        $t = 4;
        $found = false;

        do {
            if (!is_file($f = dirname($f).DIRECTORY_SEPARATOR.'composer.json')) {
                $f = dirname($f);
            } else {
                $found = true;
            }

            $t--;

        } while ($t > 0 && !$found);

        if ($found) {
            return $f;
        }
    }
}
