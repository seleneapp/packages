<?php

/**
 * This File is part of the Selene\Package\Cms\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Process;

use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Processor\ProcessInterface;
use \Selene\Package\Cms\Extension\ExtensionInterface;

/**
 * @class CollectPackageMap
 * @package Selene\Package\Cms\Process
 * @version $Id$
 */
class RegisterExtensions implements ProcessInterface
{
    public function __construct(BuilderInterface $builder)
    {
        $this->builder = $builder;
    }

    public function process(ContainerInterface $container)
    {
    }

    private function registerExtension(ExtensionInterface $extension)
    {
    }
}
