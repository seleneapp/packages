<?php

/*
 * This File is part of the Selene\Package\Cms\Resources\lang\en_EN package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

return [
    'sidebar.admin.dashboard'    => 'Dashboard',
    'sidebar.admin.settings'     => 'Settings',
    'sidebar.admin.pages.index'  => 'Pages',
    'sidebar.admin.pages.create' => 'Create New Page'
];
