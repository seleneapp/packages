<?php

/*
 * This File is part of the Selene\Package\Cms\View\Composer package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\View\Composer;

use \Selene\Module\Routing\RouterInterface;

use \Selene\Package\Cms\Menu\Builder;

/**
 * @class BreadCrump
 * @package Selene\Package\Cms\View\Composer
 * @version $Id$
 */
class BreadCrumb extends AbstractComposable
{
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    private function getNestedPath()
    {
        $tree = Builder::fromArray($this->getMenu(), 'admin.index');

        $current = $this->router->getRoutes()->get($this->router->getCurrentRoute());
        $paths   = [$this->router->getCurrentRoute() => $current];

        //var_dump($current->getParent());
        while (null !== $current->getParent()) {
            $paths[$current->getParent()] = $r = $this->router->getRoutes()->get($current->getParent());
            $current = $r;
        }

        return $paths;
    }

    protected function getMenu()
    {
        return [
            'admin.dashboard',
            'admin.settings',
            'admin.pages.index' => [
                'admin.pages.create',
                'admin.pages.show'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getContext()
    {
        return [
            'breadcrumbs' => $this->getNestedPath()
        ];
    }
}
