<?php

/*
 * This File is part of the Selene\Package\Cms\View\Composer package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\View\Composer;

use \Selene\Package\Cms\Menu\Menu;

/**
 * @class Header
 * @package Selene\Package\Cms\View\Composer
 * @version $Id$
 */
class Header extends AbstractComposable
{
    public function __construct(Menu $menu)
    {
        $this->menu = $menu;
    }

    /**
     * getContext
     *
     *
     * @access protected
     * @return mixed
     */
    protected function getContext()
    {
        return ['menu' => $this->menu];
    }
}
