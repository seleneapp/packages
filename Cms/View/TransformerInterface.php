<?php

/**
 * This File is part of the Selene\Package\Cms\View package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\View;

/**
 * @interface TransformerInterface
 * @package Selene\Package\Cms\View
 * @version $Id$
 */
interface TransformerInterface
{

}
