<?php

/*
 * This File is part of the Selene\Package\Commandeur\Command package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Commandeur\Command;

use \Thapp\Commandeur\AbstractCommand as Command;

/**
 * @class AbstractCommand
 *
 * @package Selene\Package\Commandeur\Command
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
abstract class AbstractCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    public function get($attr)
    {
        if ($this->has($attr)) {
            return parent::get($attr);
        }
    }

    /**
     * Magick getter
     *
     * @param string $attr
     *
     * @return mixed
     */
    public function __get($attr)
    {
        return $this->get($attr);
    }

    /**
     * set
     *
     * @param string $attr
     * @param mixed $value
     *
     * @return void
     */
    protected function set($attr, $value)
    {
        $this->attrs[$attr] = $value;
    }

}
