<?php

/*
 * This File is part of the Selene\Package\Commandeur\Command package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Commandeur\Command;

use \Thapp\Commandeur\CommandInterface;

/**
 * @interface NamedCommand
 *
 * @package Selene\Package\Commandeur\Command
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
interface NamedCommand extends CommandInterface
{
    /**
     * getName
     *
     * @return string
     */
    public function getName();
}
