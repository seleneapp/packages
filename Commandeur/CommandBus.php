<?php

/*
 * This File is part of the Selene\Package\Commandeur package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Commandeur;

use \Thapp\Commandeur\CommandInterface;
use \Thapp\Commandeur\CommandBusInterface;
use \Selene\Package\Commandeur\Resolver\ResolverInterface;

/**
 * @class CommandBus
 *
 * @package Selene\Package\Commandeur
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class CommandBus implements CommandBusInterface
{
    /**
     * resolver
     *
     * @var ResolverInterface
     */
    private $resolver;

    /**
     * Constructor.
     *
     * @param ResolverInterface $resolver
     */
    public function __construct(ResolverInterface $resolver)
    {
        $this->resolver = $resolver;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(CommandInterface $command)
    {
        $handler = $this->getCommandHandler($command);

        try {
            $handler->handle($command);
        } catch (\Exception $e) {

        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getCommandHanler(CommandInterface $command)
    {
        return $this->resolver->resolve($command);
    }

    public function getCommandHandler(CommandInterface $command)
    {
        return $this->resolver->resolve($command);
    }
}
