<?php

/*
 * This File is part of the Selene\Package\Commandeur package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Commandeur;

use \Selene\Module\Package\Package;

/**
 * @class CommandeurPackage
 *
 * @package Selene\Package\Commandeur
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class CommandeurPackage extends Package
{
    /**
     * {@inheritdoc}
     */
    public function requires()
    {
        return ['framework'];
    }
}
