<?php

/*
 * This File is part of the Selene\Package\Commandeur package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Commandeur;

use \Selene\Module\DI\BuilderInterface;
use \Selene\Module\Package\PackageConfiguration;

/**
 * @class Config
 *
 * @package Selene\Package\Commandeur
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Config extends PackageConfiguration
{
    /**
     * {@inheritdoc}
     */
    public function setup(BuilderInterface $builder, array $config)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return ['services.xml'];
    }
}
