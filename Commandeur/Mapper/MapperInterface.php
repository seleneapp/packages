<?php

/*
 * This File is part of the Selene\Package\Commandeur\Mapper package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Commandeur\Mapper;

/**
 * @class MapperInterface
 *
 * @package Selene\Package\Commandeur\Mapper
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
interface MapperInterface
{
    public function map($name);
}
