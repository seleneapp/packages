<?php

/*
 * This File is part of the Selene\Package\Commandeur\Resolver package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Commandeur\Resolver;

use \Thapp\Commandeur\CommandInterface;
use \Selene\Module\DI\ContainerInterface;
use \Selene\Package\Commandeur\Command\NamedCommand;
use \Selene\Package\Commandeur\Mapper\MapperInterface;

/**
 * @class ContainerResolver
 *
 * @package Selene\Package\Commandeur\Resolver
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class ContainerResolver implements ResolverInterface
{
    private $mapper;
    private $container;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     * @param MapperInterface $mapper
     */
    public function __construct(ContainerInterface $container, MapperInterface $mapper = null)
    {
        $this->container = $container;
        $this->mapper = $mapper;
    }

    /**
     * resolve
     *
     * @param CommandInterface $command
     *
     * @return CommandHandlerInterface
     */
    public function resolve(CommandInterface $command)
    {
        if ($command instanceof NamedCommand) {
            $name = $this->getHandlerName($command->getName());
        } else {
            $name = $this->getCommandHandlerNameFromClass($command);
        }

        if (!$this->container->has($name)) {
            throw new \InvalidArgumentException(sprintf('Commandhandler %s doesn\'t exits.', $name));
        }

        return $this->container->get($name);
    }

    /**
     * getCommandNamespaceAndClass
     *
     * @param CommandInterface $command
     *
     * @return string
     */
    private function getCommandHandlerNameFromClass(CommandInterface $command)
    {
        $class = get_class($command);

        list ($namespace, $name) = substr_count($class, '\\') ?
            [substr($class, 0, $pos = strrpos($class, '\\')), substr($class, $pos + 1)] :
            [null, $class];

        return $this->getHandlerName($name);
    }

    /**
     * getHandlerName
     *
     * @param string $commandName
     *
     * @return string
     */
    private function getHandlerName($commandName)
    {
        if (0 === strcasecmp($commandName, (substr($commandName, -7)))) {
            $commandName = substr($commandName, 0, -7);
        }

        return strtolower(rtrim($commandName, '._') . '.handler');
    }
}
