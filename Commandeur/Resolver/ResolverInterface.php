<?php

/*
 * This File is part of the Selene\Package\Commandeur\Resolver package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Commandeur\Resolver;

use \Thapp\Commandeur\CommandInterface;

/**
 * @interface ResolverInterface
 *
 * @package Selene\Package\Commandeur\Resolver
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
interface ResolverInterface
{
    public function resolve(CommandInterface $command);
}
