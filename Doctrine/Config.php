<?php

/**
 * This File is part of the Selene\Package\Doctrine\Config package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Doctrine;

use \Selene\Module\DI\BuilderInterface;
use \Selene\Module\DI\Reference;
use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\CallerReference;
use \Selene\Module\DI\Definition\ParentDefinition;
use \Selene\Module\DI\Definition\ServiceDefinition;
use \Selene\Module\Config\Resource\Locator;
use \Selene\Module\Config\Validator\Nodes\RootNode;
use \Selene\Module\Package\PackageConfiguration;
use \Selene\Module\Common\Helper\ListHelper;

/**
 * @class Config extends PackageConfiguration
 * @see PackageConfiguration
 *
 * @package Selene\Package\Doctrine\Config
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Config extends PackageConfiguration
{
    /**
     * {@inheritdoc}
     */
    public function setup(BuilderInterface $builder, array $config)
    {
        $this->prepareDbal($builder->getContainer(), $config['dbal']);
        $this->prepareOrm($builder->getContainer(), $config['orm']);
        $this->prepareManagerAliases($config['orm']);
    }

    /**
     * prepareDbal
     *
     * @param ContainerInterface $container
     * @param array $config
     *
     * @return void
     */
    private function prepareDbal(ContainerInterface $container, array $config)
    {
        $connections = $this->getParameter('database.pdo.connections');

        $this->setParameter(
            'doctrine.dbal.connection_default',
            $connections[$defCn = $config['default_connection']]
        );

        $doctrineConnections = [];

        $parent = $container->getDefinition('doctrine.dbal.connection_abstract');

        foreach ($connections as $name => $connection) {

            $def = new ParentDefinition('doctrine.dbal.connection_abstract');
            $def->replaceArgument($this->getDbalConnection($connection), 0);

            $container->setDefinition($id = 'doctrine.dbal.connection_' . $name, $def);

            $doctrineConnections[$name] = $id;
        }

        $this->setParameter('doctrine.dbal.connections', $doctrineConnections);

        $container->setAlias('doctrine.dbal.connection', 'doctrine.dbal.connection_default');
    }

    /**
     * getDbalConnection
     *
     * @param array $config
     *
     * @return array
     */
    private function getDbalConnection(array $config)
    {
        $config['dbname'] = $config['database'];
        $config['driver'] = $this->getDbDriverName($config['type']);

        unset($config['type']);
        unset($config['database']);

        return $config;
    }

    /**
     * prepareOrm
     *
     * @param ContainerInterface $container
     * @param array $config
     *
     * @return void
     */
    private function prepareOrm(ContainerInterface $container, array $config)
    {
        $managers = [];

        $default = $config['default_entity_manager'];
        $aliases = [];

        foreach ($config['entity_managers'] as $manager) {
            $managers[$name = $manager['name']] = $id = 'doctrine.orm.entity_manager_'.$name;

            $conn = null !== $manager['connection'] ? $manager['connection'] :
                $this->getParameter('doctrine.dbal.connection_default');

            $def = new ParentDefinition('doctrine.orm.entity_manager.abstract');
            $def->replaceArgument(new Reference($ref = 'doctrine.dbal.connection_'. $conn), 0);

            $container->setDefinition($id, $def);

            $id = 'doctrine.orm.configuration_'.$name;
            $conf = new ParentDefinition('doctrine.orm.configuration.abstract');
            $conf->replaceArgument('%doctrine.orm.meta_paths_'.$name.'%', 0);

            $def->replaceArgument(new Reference($id), 1);
            $container->setDefinition($id, $conf);

            foreach ($manager['aliases'] as $alias) {
                $aliases[$alias] = $name;
            }
        }

        //$container->removeDefinition('doctrine.orm.entity_manager');
        $container->setAlias('doctrine.orm.entity_manager', 'doctrine.orm.entity_manager_'.$default);

        $this->setParameter('doctrine.orm.entity_manager_default', $default);
        $this->setParameter('doctrine.orm.entity_managers', $managers);
        $this->setParameter('doctrine.orm.default_entity_manager', $default);
        //$this->setParameter('doctrine.orm.manager_packages', $packages);
        $this->setParameter('doctrine.orm.manager_namespace_aliases', $aliases);

        //$this->setParameter(
            //'doctrine.orm.meta_data',
            //realpath($container->getParameters()->resolveParam($config['meta_data']))
        //);

        $this->setParameter(
            'doctrine.orm.proxy_path',
            $container->getParameters()->resolveParam($config['proxy_path'])
        );

        //var_dump($config['meta_data']);
        //die;
    }

    private function prepareManagerAliases(array $config)
    {
        $managers = $config['entity_managers'];
        $aliases  = [];

        foreach ($managers as $manager) {
            foreach ($manager['aliases'] as $alias) {
                $aliases[$alias] = $manager['name'];
            }
        }
    }

    private function getDbDriverName($type)
    {
        if ('postgres' === $type) {
            $type = 'pgsql';
        }

        return 'pdo_'.$type;
    }


    public function getServiceReference($driver)
    {
        return new Reference('doctrine.cache');
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(RootNode $rootNode)
    {
        $rootNode
            // cache section:
            ->string('cache')
                ->condition()
                    ->when(function ($val) {
                        return 'null' === $val;
                    })
                    ->then(function () {
                        return null;
                    })
                ->end()
                ->condition()
                    ->ifNotInArray(['memcached', 'file', 'acp', 'null'])
                    ->thenMarkInvalid()
                ->end()
            ->end()
            // debal section:
            ->dict('dbal')
                ->string('default_connection')
                    ->optional()
                    ->defaultValue($this->getParameter('database.pdo.default_connection'))
                    ->condition()
                        ->ifNotInArray(array_keys($this->getParameter('database.pdo.connections')))
                        ->thenMarkInvalid('Doctrine DBAL: Invalid default connection "%s".')
                    ->end()
                ->end()
            ->end()
            // orm section:
            ->append($this->getCfgOrmSection())
            ->end();
    }

    /**
     * getCfgOrmSection
     *
     *
     * @return TreeBuilder
     */
    private function getCfgOrmSection()
    {
        $builder = $this->newValidatorBuilder('orm');
        $builder->getRoot()
            ->string('proxy_path')->end()
            ->dict('meta_data')
                ->values('namespaces')
                    ->dict()
                        ->string('path')->end()
                        ->string('alias')->end()
                        ->string('namespace')->end()
                    ->end()
                ->end()
            ->end()
            ->string('default_entity_manager')->notEmpty()->end()
            // managers:
            ->values('entity_managers')
                ->condition()
                    ->when(function ($value) {
                        return in_array('abstract', ListHelper::arrayPluck('name', $value));
                    })->thenMarkInvalid('Doctrine ORM: \'abstract\' is a reserved key.')
                ->end()
                ->condition()
                    ->when(function ($value, $node) {
                        $default = $node->getParent()->getChildren()->find(function ($child) {
                            return 'default_entity_manager' === $child->getKey();
                        });

                        return !in_array($default->getValue(), ListHelper::arrayPluck('name', $value));
                    })->thenMarkInvalid('Doctrine ORM: No default entity manager in %s.')
                ->end()
                ->dict()
                    ->string('name')->notEmpty()
                    ->end()
                    ->string('connection')
                        ->condition()
                            ->ifMissing()
                            ->then(function ($val, $node) {
                                $val = $node->getRoot()->getValue();

                                return $val['dbal']['default_connection'];
                            })
                        ->end()
                    ->end()
                    // ---
                    ->values('aliases')->optional()
                        ->condition()
                            ->ifMissing()
                            ->then(function ($val, $node) {
                                return [];
                                //return $this->getDefaultManagerEntityAlias($node);
                            })
                        ->end()
                        ->string()->end()
                    ->end()
                    // ---
                ->end()
            ->end();

        return $builder->getRoot();
    }

    private function getDefaultManagerEntityAlias($node)
    {
        do {
            $node = $node->getParent();
        } while ('orm' !== $node->getKey());

        $node = $node->getChildren()->find(function ($node) {
            return 'meta_data' === $node->getKey();
        });

        $value = $node->getValue();
        $namespaces = [];

        foreach ($value['namespaces'] as $ns) {
            $namespaces[] = $ns['alias'];
        }

        return $namespaces;
    }

    /**
     * prepareDefaultManager
     *
     * @param array $config
     * @param string $alias
     *
     * @return void
     */
    private function prepareDefaultManager(array $config, $alias)
    {
        $this->setParameter(
            $alias.'.orm.default_entity_manager',
            $this->getDefault($config, 'default_entity_manager', 'default')
        );
    }

    /**
     * prepareEntityManagers
     *
     * @param array $config
     * @param string $alias
     *
     * @return void
     */
    private function prepareEntityManagers(array $config, $alias)
    {
        $managers = [];

        foreach ($this->getDefault($config, 'entity_managers') as $name => $manager) {
            if (is_int($name) && !isset($manager['name'])) {
                throw new \InvalidArgumentException('Manager name must be given');
            } elseif (is_int($name)) {
                $name = $manager['name'];
                unset($manager['name']);
            }

            $managers[$name] = $manager;
        }

        $this->setParameter($alias.'.orm.entity_managers', $managers);
    }

    /**
     * prepareConnections
     *
     * @param ContainerInterface $container
     * @param array $config
     * @param string $alias
     *
     * @return void
     */
    private function prepareConnections(ContainerInterface $container, array $config, $alias)
    {
        $managers = $this->getParameter($alias.'.orm.entity_managers');
        $default  = $this->getParameter($alias.'.orm.default_entity_manager');
        $manager  = $this->getDefaultArray($managers, $default, []);

        $this->validateConnections($alias, $managers, $manager, $default);

        $credentials = [];

        // prepare credentials
        foreach ($managers as $name => $mngr) {
            $this->setParameter(
                $credentials[$name] = $alias.'.dbal.connection_'.$name.'.credentials',
                $this->getDefault($this->getParameter('database.pdo.connections'), $mngr['connection'])
            );
        }

        $container->getDefinition($alias.'.dbal.connection_default')
            ->replaceArgument($credentials[$default], 0);

        unset($credentials[$default]);

        foreach ($credentials as $name => $param) {
            $container->define($alias.'.dbal.connection_'.$name, '%'.$alias.'.dbal.connection.class%')
                ->addArgument('%'.$param.'%');
        }
    }

    /**
     * validateConnections
     *
     * @param string $alias
     * @param array $managers
     * @param array $manager
     * @param string $default
     *
     * @return void
     */
    private function validateConnections($alias, array &$managers, array &$manager, $default)
    {
        $connections = $this->getParameter('database.pdo.connections');

        if (!in_array($default, array_keys($managers))) {
            $managers[$default] = $manager;
        }

        if (!isset($manager['connection'])) {
            $manager['connection'] = $this->getParameter('database.pdo.default');
            $managers[$default] = $manager;
        }

        foreach ($managers as $name => $mngr) {

            if (!isset($mngr['connection'])) {
                throw new \InvalidArgumentException(
                    sprintf('No connection given for Entity Manager "%s".', $name)
                );
            }

            if (!in_array($connection = $mngr['connection'], array_keys($connections))) {
                throw new \InvalidArgumentException(
                    sprintf('Connection "%s" required by entity manager "%s" doesn\'t exist.', $connection, $name)
                );
            }
        }

        $this->setParameter($alias.'.orm.entity_managers', $managers);
    }
}
