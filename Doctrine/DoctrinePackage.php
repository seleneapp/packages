<?php

/**
 * This File is part of the Selene\Package\Doctrine package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Doctrine;

use \Selene\Module\DI\BuilderInterface;
use \Selene\Module\DI\Processor\ProcessorInterface;
use \Selene\Module\Package\Package;
use \Selene\Module\Package\ExportResourceInterface;
use \Selene\Module\Package\FileRepositoryInterface;
use \Selene\Adapter\Console\Application as Console;
use \Doctrine\DBAL\Migrations\Tools\Console\Command\DiffCommand;
use \Doctrine\DBAL\Migrations\Tools\Console\Command\ExecuteCommand;
use \Doctrine\DBAL\Migrations\Tools\Console\Command\GenerateCommand;
use \Doctrine\DBAL\Migrations\Tools\Console\Command\MigrateCommand;
use \Doctrine\DBAL\Migrations\Tools\Console\Command\StatusCommand;
use \Doctrine\DBAL\Migrations\Tools\Console\Command\VersionCommand;


use \Selene\Package\Doctrine\Process\ValidateConnections;
use \Selene\Package\Doctrine\Process\ProcessPackageMappings;
use \Selene\Package\Doctrine\Process\ResolveConnections;


/**
 * @class DoctrinePackage extends Package DoctrinePackage
 * @see Package
 *
 * @package Selene\Package\Doctrine
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class DoctrinePackage extends Package implements ExportResourceInterface
{
    /**
     * {@inheritdoc}
     */
    public function registerCommands(Console $console)
    {
        $this->registerMigrationCommands($console);
    }

    public function build(BuilderInterface $builder)
    {
        $builder->getProcessor()
            //->add(new ProcessPackageMappings, ProcessorInterface::BEFORE_OPTIMIZE)
            ->add(new ResolveConnections, ProcessorInterface::OPTIMIZE);
    }

    /**
     * {@inheritdoc}
     */
    protected function registerMigrationCommands(Console $console)
    {
        $console->add(new DiffCommand);
        $console->add(new ExecuteCommand);
        $console->add(new GenerateCommand);
        $console->add(new MigrateCommand);
        $console->add(new StatusCommand);
        $console->add(new VersionCommand);
    }

    /**
     * {@inheritdoc}
     */
    public function getExports(FileRepositoryInterface $files)
    {
        $files->createTarget($this->getResourcePath().'/publish/config.xml');
        $files->createTarget($this->getResourcePath().'/publish/config_development.xml');
    }

    /**
     * {@inheritdoc}
     */
    public function requires()
    {
        return ['framework'];
    }
}
