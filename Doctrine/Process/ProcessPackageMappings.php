<?php

/**
 * This File is part of the Selene\Package\Doctrine\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Doctrine\Process;

use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Definition\ServiceDefinition;
use \Selene\Module\DI\Processor\ProcessInterface;
use \Selene\Module\Package\PackageRepositoryInterface;
use \Doctrine\ORM\Tools\Setup;

/**
 * @class ProcessPackageMappings
 * @package Selene\Package\Doctrine\Process
 * @version $Id$
 */
class ProcessPackageMappings implements ProcessInterface
{
    /**
     * packages
     *
     * @var PackageRepositoryInterface
     */
    private $packages;

    /**
     * container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * Constructor.
     *
     * @param PackageRepository $packages
     */
    //public function __construct(PackageRepositoryInterface $packages)
    //{
        //$this->packages = $packages;
    //}

    public function process(ContainerInterface $container)
    {
        $this->container = $container;

        list ($paths, $namespaces) = $this->getPathsAndNamespaces($container->get('app')->getPackages());

        //$meta     = $container->getParameter('doctrine.orm.meta_data');
        $managers = $container->getParameter('doctrine.orm.manager_packages');

        foreach ($managers as $manager => $packages) {

        }

        //$p = $this->filterPackagesForConfig($mngr, $names, $entities, $meta);

        //$container->getDefinition('doctrine.orm.configuration_'.$mngr));

        //$container->getParameters()->remove('doctrine.orm.manager_packages');
        //die;
    }

    private function getPathsAndNamespaces($packages)
    {
        foreach ($packages as $alias => $package) {
            if (is_dir($path = $package->getPath().DIRECTORY_SEPARATOR.'Entity')) {
                $entities[$alias]   = $path;
                $namespaces[$alias] = $package->getNamespace() . '\Entity';
            }
        }

        return [$entities, $namespaces];
    }

    /**
     * filterPackagesForConfig
     *
     * @return array
     */
    private function filterPackagesForConfig($manager, array $names, array $paths, $defaultPath)
    {
        if (empty($names)) {
            return [$defaultPath];
        }

        $p = [];

        array_filter($names, function ($name) use (&$paths, &$p) {
            if (array_key_exists($name, $paths)) {
                $p[] = $paths[$name];

                return true;
            }

            return false;
        });

        if (count($p) === count($names)) {
            return $p;
        }

        throw new \InvalidArgumentException(
            sprintf(
                'Doctrine ORM: invalid packages %s for EntityManager %s',
                implode(', ', array_diff($names, $p)),
                $manager
            )
        );
    }
}
