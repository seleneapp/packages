<?php

/*
 * This File is part of the Selene\Package\Doctrine package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Doctrine\Process;

use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Definition\ServiceDefinition;
use \Selene\Module\DI\Processor\ProcessInterface;

/**
 * @class ResolveConnections
 *
 * @package Selene\Package\Doctrine
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class ResolveConnections implements ProcessInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerInterface $container)
    {
        $this->container = $container;
        $params = $container->getParameters();

        $aliases = $params->resolveParam($params->get('doctrine.orm.manager_namespace_aliases'));
        $nsAliases = $this->container->getParameter('app.namespace_aliases');
        $nsPaths   = $this->container->getParameter('app.namespace_paths');

        foreach ($aliases as $alias => $name) {
            if (!array_key_exists($alias, $nsAliases)) {
                throw new \InvalidArgumentException(
                    sprintf('No namespace alias "%s" found for Manager "%s".', $alias, $name)
                );
            }
        }

        $found = [];

        foreach ($container->getParameter('doctrine.orm.entity_managers') as $name => $id) {
            $found[$name] = $this->getEntityNamespaces($name, $aliases, $nsAliases);
        }

        $this->setEntityNamespacesAndPaths($this->filterEntityNamespaces($found, $nsAliases), $nsPaths);
    }

    /**
     * setEntityNamespacesAndPaths
     *
     * @param array $namespaces
     * @param array $paths
     *
     * @return void
     */
    private function setEntityNamespacesAndPaths(array $namespaces, array $paths)
    {
        $entityPaths = [];

        foreach ($namespaces as $mngr => $aliases) {
            $args  = $this->container->getDefinition('doctrine.orm.entity_manager_'.$mngr)->getArguments();
            $conf = $this->container->getDefinition($args[1]);

            // set aliases
            $conf->addSetter('setEntityNamespaces', [
                array_map(function ($ns) {
                    return $ns . '\Entity';
                }, $aliases)
            ]);
            // set paths:
            //$p = [];

            //foreach (array_intersect_key($paths, array_flip($aliases)) as $key => $pp) {
                //$p[$key . '\\Entity'] = $pp . DIRECTORY_SEPARATOR . 'Entity';
            //}

            $conf->replaceArgument(array_map(function ($path) {
                return $path . DIRECTORY_SEPARATOR . 'Entity';
            }, array_intersect_key($paths, array_flip($aliases))), 0);
        }
    }

    /**
     * filterEntityNamespaces
     *
     * @param mixed $found
     * @param array $nsAliases
     *
     * @return array
     */
    private function filterEntityNamespaces($found, array $nsAliases)
    {
        $res = [];
        $final = [];

        foreach ($found as $name => $aliases) {
            $final[$name] = [];

            foreach ($aliases as $alias => $namespace) {
                $res[$alias] = $name;
            }
        }

        foreach ($res as $alias => $mngr) {
            $final[$mngr][$alias] = $nsAliases[$alias];
        }

        return $final;
    }

    /**
     * getEntityNamespaces
     *
     * @param mixed $name
     * @param array $aliases
     * @param array $nsAliases
     *
     * @return array
     */
    private function getEntityNamespaces($name, array $aliases, array $nsAliases)
    {
        $aliases = array_keys(array_filter($aliases, function ($mngr) use ($name) {
            return $name === $mngr;
        }));

        if (empty($aliases)) {
            $aliases = $nsAliases;
        } else {
            $aliases = array_intersect_key($nsAliases, array_flip($aliases));
        }

        return $aliases;
    }
}
