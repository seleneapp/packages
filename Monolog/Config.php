<?php

/**
 * This File is part of the Config package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Monolog;

use \Selene\Module\DI\Reference;
use \Selene\Module\DI\Loader\XmlLoader;
use \Selene\Module\DI\BuilderInterface;
use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Definition\ServiceDefinition;
use \Selene\Module\Config\Resource\Locator;
use \Selene\Module\Config\Resource\LocatorInterface;
use \Selene\Module\Package\PackageInterface;
use \Selene\Module\Package\PackageConfiguration;
use \Selene\Module\Config\Validator\Nodes\DictNode;
use \Selene\Module\Config\Validator\Nodes\RootNode;

/**
 * @class Config
 * @package Config
 * @version $Id$
 */
class Config extends PackageConfiguration
{
    /**
     * {@inheritdoc}
     */
    public function setup(BuilderInterface $builder, array $config)
    {
        $this->setupStreamHandlers($builder->getContainer(), $config);
    }

    /**
     * setupStreamHandlers
     *
     * @param ContainerInterface $container
     * @param array $config
     *
     * @return void
     */
    protected function setupStreamHandlers(ContainerInterface $container, array $config)
    {
        $defaultHandler = $this->setDefaultHandler($container, $config);
    }

    /**
     * setDefaultHandler
     *
     * @param ContainerInterface $container
     * @param array $config
     *
     * @return void
     */
    protected function setDefaultHandler(ContainerInterface $container, array $config)
    {
        $alias    = $this->getPackageAlias();
        $default  = $this->getDefault($config, 'default_handler');
        $handlers = $this->getDefault($config, 'handlers', []);

        // if no default handler is given, set the factory default handler.
        $defaultHandler = $this->getHandler($handlers, $default);

        $defaultHandlerClass = $this->getHandlerClass(
            $type = $this->getDefault($defaultHandler, 'type', 'file'),
            $alias
        );

        $this->setParameter($alias.'.handler.class', $defaultHandlerClass);

        if (!$container->hasDefinition($id = $alias.'.handler_default')) {
            $handler = new ServiceDefinition;
        } else {
            $handler = $container->getDefinition($id);
        }

        if (isset($config['name'])) {
            $this->setParameter($alias.'.logger_name', $config['name']);
        }

        $handler->setClass($defaultHandlerClass);
        $handler->setInternal(true);


        $name = $this->getParameter($alias.'.logger_name');

        if ('file' === $type) {
            $handler->setArguments([
                $this->getDefaultLogPath($defaultHandler)
            ]);
        } elseif ('syslog' === $type) {
            $handler->setArguments([
                $this->getDefault($defaultHandler, 'ident', $alias)
            ]);
        } else {
            $handler->setArguments([]);
        }

        $logger = $container->getDefinition($alias);
        $logger->setArguments([$name, [new Reference($id)]]);
    }

    protected function getDefaultLogPath($handler)
    {
        return $this->getDefaultUsing($handler, 'path', function () {
            return $this->getParameter('monolog.default_handler.path', '%app.path%/log/info.log');
        });
    }

    /**
     * getHandlerClass
     *
     * @param string $type
     * @param string $alias
     *
     * @return string
     */
    protected function getHandlerClass($type, $alias)
    {
        switch ($type) {
            case 'file':
                return $this->getParameter(
                    $alias.'.handler_file.class',
                    'Monolog\Handler\StreamHandler'
                );
            case 'syslog':
                return $this->getParameter(
                    $alias.'.handler_syslog.class',
                    'Monolog\Handler\SyslogHandler'
                );
            case 'php_error':
                return $this->getParameter(
                    $alias.'.handler_errorlog.class',
                    'Monolog\Handler\ErrorLogHandler'
                );
            default:
                return $this->getParameter(
                    $alias.'.handler_default.class',
                    'Monolog\Handler\StreamHandler'
                );
        }
    }

    /**
     * getHandler
     *
     * @param array $handlers
     * @param mixed $name
     *
     * @return void
     */
    private function getHandler(array $handlers, $name = null)
    {
        $default = [
            'name' => 'default_handler',
            'type' => 'file'
        ];

        if (null === $name) {
            return $default;
        }

        foreach ($handlers as $handler) {
            if (isset($handler['name']) && $name === $handler['name']) {
                return $handler;
            }
        }

        return $default;
    }

    protected function getConfigLoader(BuilderInterface $builder, LocatorInterface $locator = null)
    {
        $locator = $locator ?: new Locator([$this->getResourcePath()]);

        return new XmlLoader($builder, $locator);
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigTree(RootNode $rootNode)
    {
        $levels = ['info', 'error', 'debug'];
        $handlers = ['php_error', 'file', 'syslog'];

        $alias = $this->getPackageAlias();
        $rootNode
            ->string('name')->optional()->defaultValue('monolog')->end()
            ->string('min_log_level')->optional()->defaultValue('debug')
                ->condition()
                    ->ifNotInArray($levels)
                    ->thenMarkInvalid()
                    ->end()
                ->end()
            ->values('log_levels')->notEmpty()
                ->dict()
                    ->string('handler')
                        ->end()
                    ->string('level')
                        ->end()
                    ->end()
                ->end()
            ->string('default_handler')->optional()->defaultValue('debug')
                ->end()
            ->values('handlers')
                ->dict(null, DictNode::KEYS_STRICT)
                    ->string('name')->end()
                    ->string('type')
                        ->condition()
                            ->when(function ($val) {
                                return 'file' === $val;
                            })
                            ->then(function ($key, $node) {
                                $class = get_class($node);
                                $value  = $node->getParent()->getValue();
                                $node->getParent()->addChild($str = new $class);
                                $str->setKey('path');
                                $str->notEmpty()
                                    ->finalize(isset($value['path']) ? $value['path'] : null)
                                    ->validate();
                            })
                            ->end()
                        ->condition()
                            ->when(function ($val) {
                                //var_dump($val);
                                return 'syslog' === $val;
                            })
                            ->then(function ($key, $node) {
                                $class = get_class($node);
                                $value = $node->getParent()->getValue();
                                $node->getParent()->addChild($str = new $class);
                                $str->setKey('ident');
                                $str
                                    ->notEmpty()
                                    ->finalize(isset($value['ident']) ? $value['ident'] : null)
                                    ->validate();
                            })
                            ->end()
                        ->end()
                    ->end();
    }

    /**
     * getResources
     *
     * @return array
     */
    protected function getResources()
    {
        return ['services.xml'];
    }
}
