<?php

/*
 * This File is part of the Selene\Package\Twig package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Twig\Process;

use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Processor\ProcessInterface;

/**
 * @class EnsureCacheLocation
 * @package Selene\Package\Twig
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class EnsureCacheLocation implements ProcessInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerInterface $container)
    {
        // ensure cache directory exists
        $parameters = $container->getParameters();

        $opts = $parameters->get('twig.options');

        if (!isset($opts['cache'])) {
            throw new \InvalidArgumentException('No cache path found');
        }

        if ($cacheDir = $parameters->resolveParam($opts['cache'])) {
            if (!is_dir($cacheDir)) {
                @mkdir($cacheDir, 0777, true);
            }
        }
    }
}
