<?php

/**
 * This File is part of the Selene\Package\Twig package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Twig\Process;

use \Selene\Module\DI\Reference;
use \Selene\Module\DI\CallerReference;
use \Selene\Module\DI\ContainerInterface;
use \Selene\Adapter\Twig\Process\PrepareExtensions as TwigPrepareExtensions;

/*
 * @class PrepareExtensions extends TwigPrepareExtensions
 * @see TwigPrepareExtensions
 *
 * @package Selene\Package\Twig
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class PrepareExtensions extends TwigPrepareExtensions
{
    /**
     * Constructor.
     *
     * Set the appropriated names.
     */
    public function __construct($tag = null, $extensions = self::EXT_ALL)
    {
        $this->setContainerId('app.container');

        parent::__construct('twig.extension', []);
    }

    /**
     * {@inheritdoc}
     */
    protected function getInternalArguments($name)
    {
        switch ($name) {
            case 'selene_di_parameters':
                return [new CallerReference($this->getContainerId(), 'getParameters')];
            case 'selene_routing_url':
                return [new Reference('routing.url')];
            default:
                return [];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function process(ContainerInterface $container)
    {
        $this->setExtensions(
            $dd = $container->getParameters()->get('twig.extensions_default')
        );

        $container->getParameters()->remove('twig.extensions_default');

        parent::process($container);
    }
}
