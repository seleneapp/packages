<?php

/*
 * This File is part of the Selene\Package\Twig package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Twig\Process;

use \Selene\Module\DI\Reference;
use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Definition\FlagInterface;
use \Selene\Module\DI\Processor\ProcessInterface;
use \Selene\Adapter\Twig\Process\RegisterExtensions as TwigRegisterExtensions;

/**
 * @class RegisterExtensions extends TwigRegisterExtensions
 * @see TwigRegisterExtensions
 *
 * @package Selene\Package\Twig
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class RegisterExtensions extends TwigRegisterExtensions
{
    /**
     * Constructor.
     *
     * Set the appropriated names.
     */
    public function __construct()
    {
        $this->setTwigEnvId('twig.env');
        $this->setTwigExtensionMetaName('twig.extension');
    }
}
