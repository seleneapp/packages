<?php

/*
 * This File is part of the Selene\Package\Twig\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Twig\Process;

use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Processor\ProcessInterface;

/**
 * @class RemoveObsoleteParameters
 * @package Selene\Package\Twig\Process
 * @version $Id$
 */
class RemoveObsoleteParameters implements ProcessInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerInterface $container)
    {
        foreach ($this->getObsolete() as $param) {
            $container->getParameters()->remove($param);
        }
    }

    /**
     * getObsolete
     *
     * @return array
     */
    private function getObsolete()
    {
        return [
            'twig.extensions_default',
            'twig.options'
        ];
    }
}
