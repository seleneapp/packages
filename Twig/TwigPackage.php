<?php

/*
 * This File is part of the Selene\Package\Twig package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Twig;

use \Selene\Module\Package\Package;
use \Selene\Module\DI\BuilderInterface;
use \Selene\Module\DI\Processor\ProcessorInterface;
use \Selene\Package\Twig\Process\RegisterEngine;
use \Selene\Package\Twig\Process\RegisterExtensions;
use \Selene\Package\Twig\Process\EnsureCacheLocation;
use \Selene\Package\Twig\Process\PrepareExtensions;
use \Selene\Package\Twig\Process\RemoveObsoleteParameters;

/**
 * @class TwigPackage extends Package TwigPackage
 * @see Package
 *
 * @package Selene\Package\Twig
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class TwigPackage extends Package
{
    public function build(BuilderInterface $builder)
    {
        $builder->getProcessor()
            ->add(new PrepareExtensions, ProcessorInterface::OPTIMIZE)
            ->add(new RegisterExtensions, ProcessorInterface::OPTIMIZE)
            ->add(new EnsureCacheLocation, ProcessorInterface::OPTIMIZE)
            ->add(new RemoveObsoleteParameters, ProcessorInterface::REMOVE);
    }

    /**
     * {@inheritdoc}
     */
    public function requires()
    {
        return ['framework'];
    }
}
