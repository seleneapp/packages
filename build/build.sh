#!/bin/sh

WHICHGIT=`which git`
GIT=`echo $WHICHGIT`

$GIT subsplit init git@github.com:seleneapp/packages.git

$GIT subsplit publish --no-tags Cms:git@github.com:seleneapp/cms.git
$GIT subsplit publish --no-tags Monolog:git@github.com:seleneapp/monolog-package.git
$GIT subsplit publish --no-tags Twig:git@github.com:seleneapp/twig-package.git

rm -rf ./.subsplit
